# Feed Handler
# Copyright © 2009 Thomas Perl <thp@thpinfo.com>

APP=feedhandler
OBJS=feedhandler.o
PACKAGES=glib-2.0 dbus-glib-1 gtk+-2.0 libosso pango

DESTDIR ?= /
PREFIX ?= /usr

DESKTOP=feedhandler.desktop
SERVICE=feedhandler.service

FEEDHANDLER=$(PREFIX)/bin/$(APP)
DBUSSERVICE=$(PREFIX)/share/dbus-1/services/org.maemo.garage.feedhandler.service
DESKTOPFILE=$(PREFIX)/share/applications/hildon/zzz-feedhandler.desktop

VALAFLAGS=$(addprefix --pkg=,$(PACKAGES))
CFLAGS=`pkg-config --cflags $(PACKAGES)`
LDFLAGS=`pkg-config --libs $(PACKAGES)`

all: $(APP)

$(APP): $(OBJS)

%.c: %.vala
	valac $(VALAFLAGS) --ccode $<
	# Workaround for Vala bug 595578
	sed -i -e 's/MimeOpen/mime_open/g' $@

install: $(APP)
	install -D -m 655 $(APP) $(DESTDIR)$(FEEDHANDLER)
	install -D -m 644 $(SERVICE) $(DESTDIR)$(DBUSSERVICE)
	install -D -m 644 $(DESKTOP) $(DESTDIR)$(DESKTOPFILE)

clean:
	rm -f $(OBJS) $(APP)

.DEFAULT: all
.PHONY: all install clean

